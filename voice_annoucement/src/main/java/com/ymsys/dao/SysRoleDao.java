package com.ymsys.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ymsys.entity.SysRoleEntity;

/**
 * 角色
 * 
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	
}

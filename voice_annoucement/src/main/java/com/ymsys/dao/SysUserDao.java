package com.ymsys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ymsys.entity.SysUserEntity;

import java.util.List;

/**
 * 系统用户
 * 
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-13 16:25:20
 */
public interface SysUserDao extends BaseMapper<SysUserEntity> {
    /**
     * 查询用户的所有权限
     * @param userId  用户ID
     */
    List<String> queryAllPerms(Long userId);

    /**
     * 查询用户的所有菜单ID
     */
    List<Long> queryAllMenuId(Long userId);
}

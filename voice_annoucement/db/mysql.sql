create database voice_annoucement;
use voice_annoucement;

-- 系统用户
create table `sys_user` (
	`user_id` bigint not null auto_increment comment '用户id',
    `user_name` varchar(50) not null comment '用户名',
    `password` varchar(100) not null comment '密码',
    `salt` varchar(20) not null comment '盐',
    `email` varchar(50) not null comment '邮箱',
    `mobile` varchar(20) not null comment '手机号',
    `status` tinyint comment '用户状态 0:禁用 1:正常',
	`create_time` datetime comment '创建时间',
	`update_time` datetime comment '更新时间',
    primary key (`user_id`),
    unique index(`user_name`,`email`,`mobile`)
)engine=InnoDB default charset=utf8 comment='系统用户';

-- 角色
create table `sys_role`(
	`role_id` bigint not null auto_increment,
    `role_name` varchar(50) comment '角色名称',
	`remark` varchar(100) comment '备注',
    `create_time` datetime comment '创建时间',
    `update_time` datetime comment '修改时间',
     primary key(`role_id`),
     unique index(`role_name`)
)engine=InnoDB default charset=utf8 comment='角色';	

-- 用户角色表
create table `sys_user_role`(
	`id` bigint not null auto_increment,
	`user_id` bigint comment '用户id',
	`role_id` bigint comment '角色id', 
	primary key(`id`),
    FOREIGN KEY (`user_id`) REFERENCES sys_user(`user_id`)ON DELETE CASCADE,
    FOREIGN KEY (`role_id`) REFERENCES sys_role (`role_id`)
)engine=InnoDB default charset=utf8 comment='用户与角色对应关系';

-- 菜单
create table `sys_menu`(
	`menu_id` bigint not null auto_increment,
	`parent_id` bigint comment '父菜单id，一级菜单为0',
	`name` varchar(50) comment '菜单名称',
	`url` varchar(200) COMMENT '菜单URL',
	`perms` varchar(500) COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
	`type` int COMMENT '类型   0：目录   1：菜单   2：按钮',
	`icon` varchar(50) COMMENT '菜单图标',
	`order_num` int COMMENT '排序',
	PRIMARY KEY (`menu_id`)
) engine=InnoDB default charset=utf8 comment='菜单管理';


-- 角色与菜单对应关系
create table `sys_role_menu` (
  `id` bigint NOT NULL auto_increment,
  `role_id` bigint COMMENT '角色ID',
  `menu_id` bigint COMMENT '菜单ID',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role_id`) REFERENCES sys_role(`role_id`)ON DELETE CASCADE,
  FOREIGN KEY (`menu_id`) REFERENCES sys_menu (`menu_id`)
)engine=InnoDB default charset=utf8 comment='角色与菜单对应关系';



-- 客户
create table `sys_customer` (
	`customer_id` varchar(50) not null,
    `company_name` varchar(50) comment '客户公司名',
    `customer_name` varchar(50) comment '客户姓名',
	`email` varchar(50) not null comment '邮箱',
    `mobile` varchar(20) not null comment '手机号',
    `create_time` datetime comment '创建时间',
	`update_time` datetime comment '更新时间',
    `balance` bigint not null default 0 comment '余额',
    `overdraft_limit` bigint not null default 0 comment '透支额度',
    `user_id` bigint not null comment '开户人'
)engine=InnoDB default charset=utf8 comment='客户管理';





-- 初始化数据

-- 超级管理员
INSERT INTO `sys_user` (`user_id`, `user_name`, `password`, `salt`, `email`, `mobile`, `status`, `create_time`,`update_time`) 
VALUES ('1', 'admin', 'e1153123d7d180ceeb820d577ff119876678732a68eef4e6ffc0b1f06a01f91b', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '2016-11-11 11:11:11','2017-11-11 11:11:11');

-- 菜单
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('1', '0', '语音通知', NULL, NULL, '0', 'fa fa-cog', '0');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('2', '1', '客户管理', 'url', NULL, '1', 'fa fa-cog', '1');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('3', '1', '号码管理', 'url', NULL, '1', 'fa fa-cog', '2');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('4', '1', '铃音管理', 'url', NULL, '1', 'fa fa-cog', '3');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('5', '1', '费率管理', 'url', NULL, '1', 'fa fa-cog', '4');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('6', '1', '财务管理', 'url', NULL, '1', 'fa fa-cog', '5');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('7', '1', '报表管理', 'url', NULL, '1', 'fa fa-cog', '6');
INSERT INTO `sys_menu` (`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES ('8', '1', '系统管理', 'url', NULL, '1', 'fa fa-cog', '7');

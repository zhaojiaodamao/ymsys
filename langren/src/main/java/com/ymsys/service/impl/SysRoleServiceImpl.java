package com.ymsys.service.impl;

import com.ymsys.dao.SysRoleDao;
import com.ymsys.entity.SysRoleEntity;
import com.ymsys.service.SysRoleService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

@Service("sysRoleService")
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleEntity> implements SysRoleService {

    /*
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<SysRoleEntity> page = this.selectPage(
                new Query<SysRoleEntity>(params).getPage(),
                new EntityWrapper<SysRoleEntity>()
        );

        return new PageUtils(page);
    }
    */

}

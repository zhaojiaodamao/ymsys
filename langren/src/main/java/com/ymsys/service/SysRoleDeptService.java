package com.ymsys.service;


import com.baomidou.mybatisplus.service.IService;
import com.ymsys.entity.SysRoleDeptEntity;

import java.util.List;

/**
 * 角色与部门对应关系
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-15 14:25:42
 */
public interface SysRoleDeptService extends IService<SysRoleDeptEntity> {

   // PageUtils queryPage(Map<String, Object> params);
    /**
     * 根据角色ID，获取部门ID列表
     */
    List<Long> queryDeptIdList(Long[] roleIds) ;

}


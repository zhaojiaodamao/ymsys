package com.ymsys.service;

import com.baomidou.mybatisplus.service.IService;
import com.ymsys.entity.SysDeptEntity;


import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 * @author zhaojiao
 * @email 616750809@qq.com
 * @date 2018-11-15 14:25:41
 */
public interface SysDeptService extends IService<SysDeptEntity> {

    List<SysDeptEntity> queryList(Map<String, Object> map);

    /**
     * 查询子部门ID列表
     * @param parentId  上级部门ID
     */
    List<Long> queryDetpIdList(Long parentId);

    /**
     * 获取子部门ID，用于数据过滤
     */
    List<Long> getSubDeptIdList(Long deptId);

}

